#!/usr/bin/env python3
import logging
import sys

from PyQt5.QtWidgets import QApplication

from config import *
from gui import Sender
from send_data import set_listener_manual_mode


def main():
    logging.basicConfig(stream=sys.stderr, format=LOG_FORMAT, level=logging.DEBUG, force=True)

    logging.debug(f"UDP target IP: {UDP_IP}")
    logging.debug(f"UDP target port: {UDP_PORT}")

    app = QApplication(sys.argv)
    window = Sender()
    window.show()
    exitcode = app.exec_()
    set_listener_manual_mode(False)  # Deactivate manual mode on exit
    sys.exit(exitcode)


if __name__ == "__main__":
    main()
