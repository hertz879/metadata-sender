import logging

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QMainWindow

from send_data import send_playlist_entry, set_listener_manual_mode


class Sender(QMainWindow):

    def __init__(self):
        QMainWindow.__init__(self)

        self.setObjectName("MainWindow")

        # status bar
        self.statusBar().setStyleSheet("color: green;")
        self.statusBar().show()

        # central widget
        self.widget = QtWidgets.QWidget(self)
        self.widget.setObjectName("widget")

        # Layout for inputs and button
        self.verticalLayout = QtWidgets.QVBoxLayout(self.widget)
        self.verticalLayout.setContentsMargins(20, 20, 20, 20)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")

        # Input for artist (with label)
        self.label_artist = QtWidgets.QLabel(self.widget)
        self.label_artist.setObjectName("label_artist")
        self.horizontalLayout.addWidget(self.label_artist)
        self.lineEdit_artist = QtWidgets.QLineEdit(self.widget)
        self.lineEdit_artist.setObjectName("lineEdit_artist")
        self.horizontalLayout.addWidget(self.lineEdit_artist)

        # Input for song (with label)
        self.label_song = QtWidgets.QLabel(self.widget)
        self.label_song.setObjectName("label_song")
        self.horizontalLayout.addWidget(self.label_song)
        self.lineEdit_song = QtWidgets.QLineEdit(self.widget)
        self.lineEdit_song.setObjectName("lineEdit_song")
        self.horizontalLayout.addWidget(self.lineEdit_song)
        self.verticalLayout.addLayout(self.horizontalLayout)

        # Button to set the playlist entry
        self.button_playlist_set = QtWidgets.QPushButton(self.widget)
        self.button_playlist_set.setObjectName("button_playlist_set")
        self.verticalLayout.addWidget(self.button_playlist_set)

        # Separated section to (de)activate manual mode #

        # Line to separate sections
        self.line = QtWidgets.QFrame(self.widget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setMinimumSize(0, 10)
        self.line.setObjectName("line")
        self.verticalLayout.addWidget(self.line)

        # Short description what the manual mode does
        self.label_manual_mode = QtWidgets.QLabel(self.widget)
        self.label_manual_mode.setObjectName("label_manual_mode")
        self.label_manual_mode.setTextFormat(QtCore.Qt.MarkdownText)
        self.verticalLayout.addWidget(self.label_manual_mode)

        # One button to each activate and deacticvate manual mode, organized in a horizontal layout
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.button_manual_activate = QtWidgets.QPushButton(self.widget)
        self.button_manual_activate.setObjectName("button_manual_activate")
        self.horizontalLayout_2.addWidget(self.button_manual_activate)
        self.button_manual_deactivate = QtWidgets.QPushButton(self.widget)
        self.button_manual_deactivate.setObjectName("button_manual_deactivate")
        self.horizontalLayout_2.addWidget(self.button_manual_deactivate)
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        # Hint at the bottom that the manual mode is disabled on close
        self.label_manual_mode_hint = QtWidgets.QLabel(self.widget)
        self.label_manual_mode_hint.setObjectName("label_manual_mode_hint")
        self.verticalLayout.addWidget(self.label_manual_mode_hint)

        self.setCentralWidget(self.widget)

        # Name things
        self.retranslate_ui()

        # Activate playlist button if return is pressed in one of the input fields
        self.lineEdit_song.returnPressed.connect(self.button_playlist_set.click)
        self.lineEdit_artist.returnPressed.connect(self.button_playlist_set.click)

        # What to do when the playlist button is pressed
        self.button_playlist_set.clicked.connect(self.playlist_set)

        # Manual mode buttons actions
        self.button_manual_activate.clicked.connect(self.manual_mode_activate)
        self.button_manual_deactivate.clicked.connect(self.manual_mode_deactivate)

        QtCore.QMetaObject.connectSlotsByName(self)

    def playlist_set(self):
        artist: str = self.lineEdit_artist.text()
        song: str = self.lineEdit_song.text()

        logging.debug(f'Button pressed to set playlist to artist "{artist}" and song "{song}"')
        send_playlist_entry(artist=artist, song=song)
        self.statusBar().showMessage(f'Playlist wurde eingestellt auf: {artist} - {song}')

    def manual_mode_activate(self):
        logging.debug("Button pressed to activate manual mode")
        set_listener_manual_mode(True)
        self.statusBar().showMessage("Manueller Modus aktiviert!")

    def manual_mode_deactivate(self):
        logging.debug("Button pressed to deactivate manual mode")
        set_listener_manual_mode(False)
        self.statusBar().showMessage("Manueller Modus deaktiviert!")

    def retranslate_ui(self):
        _translate = QtCore.QCoreApplication.translate
        self.setWindowTitle(_translate("MainWindow", "Hertz Metadata-Sender"))
        self.label_artist.setText(_translate("MainWindow", "Artist:"))
        self.lineEdit_artist.setText(_translate("MainWindow", "Hertz 87.9"))
        self.label_song.setText(_translate("MainWindow", "Song:"))
        self.lineEdit_song.setText(_translate("MainWindow", "Stromausfall"))
        self.button_playlist_set.setText(_translate("MainWindow", "Als Playlist-Eintrag setzen!"))
        self.label_manual_mode.setText(_translate("MainWindow", "**Manueller Modus:** Wenn aktiviert, "
                                                                "werden alle Playlist-Daten von Rivendell ignoriert!\n"
                                                                "\n Alles was im Studio gespielt wird, taucht dann also"
                                                                "nicht mehr in der Playlist auf."))
        self.button_manual_activate.setText(_translate("MainWindow", "Aktivieren"))
        self.button_manual_deactivate.setText(_translate("MainWindow", "Deaktivieren"))
        self.label_manual_mode_hint.setText(_translate("MainWindow", "Achtung! Wenn dieses Fenster geschlossen wird, w"
                                                                     "ird der manuelle Modus automatisch deaktiviert."))
