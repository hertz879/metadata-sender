import logging
import socket
from config import *


def send_message(message: str) -> None:
    """
    Send an UDP string to the metadata-listener.

    :param message: Message to send
    :return: None
    """
    sock: socket.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP

    logging.debug(f'Sending message to metadata-listener: "{message}"')
    sock.sendto(bytes(message, RIVENDELL_ENCODING), (UDP_IP, UDP_PORT))


def send_playlist_entry(artist: str, song: str) -> None:
    """
    Send an playlist entry to the metadata listener. The format has to match the one Rivendell uses,
    which is 'group|artist|song|duration in ms|garbage'.

    :param artist: Artist to display in playlist
    :param song: Track to display in playlist
    :return: None
    """

    message: str = f"{MANUAL_GROUP}|{artist}|{song}|0|"
    send_message(message)


def set_listener_manual_mode(state: bool) -> None:
    """
    Switch metadata-listener manual mode on or off.

    :param state: True for on, False for off
    :return: None
    """

    if state:
        send_message(MANUAL_START_STRING)
    else:
        send_message(MANUAL_STOP_STRING)
