UDP_IP = '127.0.0.1'
UDP_PORT = 5000

# We need to mimic the RD messages for the listener to undersatnd
RIVENDELL_ENCODING = 'iso-8859-15'

LOG_FORMAT = "[%(levelname)s] %(message)s"

# Sicher gehen, dass diese Daten mit dem metadata-listener identisch sind!
MANUAL_GROUP = 'MANUAL'
MANUAL_START_STRING = '%ENTERMANUAL%'
MANUAL_STOP_STRING = '%EXITMANUAL%'
