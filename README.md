# Metadata-Sender

Sends metadata to the [Metadata-Listener](https://gitlab.ub.uni-bielefeld.de/hertz879/metadata-listener), to manually add playlist entries from a GUI.
